package Adaptive;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

//Create txt file
public class CreateFile {

	public void createFile(List<String> content, String url) {
		BufferedWriter bufferedWriter = null;
		try {
			String fileName = createFileName(url);
			String CachePath = SystemConfig.CACHE_PATH;
			String path = CachePath + fileName + ".txt";
			bufferedWriter = new BufferedWriter(new FileWriter(path));
			
			for (String s : content) {
				bufferedWriter.write(s);
				// write a new line
				bufferedWriter.newLine();
				// flush
				bufferedWriter.flush();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public String createFileName(String url) {
		String fileName = url.replaceAll("[^a-zA-Z]", "");
		System.out.println(fileName);
		return fileName;
	}
	
	public String getFileName(String url) {
		String fileName = url.replaceAll("[^a-zA-Z]", "");
		return fileName;
	}

}
