package Adaptive;

import java.io.*;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.Semaphore;


public class CacheRefKeeper {
	
	private static CacheRefKeeper single_instance = null;
	private HashMap<String, Long> hm;
	private Semaphore sem;

	private CacheRefKeeper() {
		hm = new HashMap<String, Long>();
		sem = new Semaphore(1);
	}
	
	synchronized public static CacheRefKeeper getInstance() {
		if (single_instance == null)
			single_instance = new CacheRefKeeper();
		return single_instance;
	}
	
	
	public List<String> readHm(String url) throws FileNotFoundException, IOException {
		List<String> lines = new ArrayList<String>();
		CreateFile createfileOb = new CreateFile();
		String file = SystemConfig.CACHE_PATH + createfileOb.getFileName(url) + ".txt";
		
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line = "";
		while((line = br.readLine()) != null) {
			lines.add(line);
		}
		br.close();
		return lines;
	}
	
	
	public void writeHm(String url) {
		Instant instant = Instant.now();
		long timeStamp = instant.getEpochSecond();
		try {
			sem.acquire();
			hm.put(new CreateFile().getFileName(url), timeStamp);
		} catch (InterruptedException e) {
			e.printStackTrace();
			hm.put(new CreateFile().getFileName(url), timeStamp);
		}
		sem.release();
	}

	
	public int getResouce(String url) {
		String fileName = new CreateFile().getFileName(url);
		Instant instant = Instant.now();
		long timeStamp = instant.getEpochSecond();

		if (!hm.containsKey(fileName) ||
				(hm.containsKey(fileName)) && (timeStamp - hm.get(fileName) >= 60 * 60)) {
			//not exist or too old
			return 2;
		} 
		//already exist and not old
		return 1;
	}

}
