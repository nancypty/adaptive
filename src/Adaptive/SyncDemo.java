package Adaptive;

import java.util.ArrayList;
import java.util.List;

public class SyncDemo {
	public static void main(String args[])
	{   
		List<String> keywords1 = new ArrayList<String>();
		keywords1.add("google");
		keywords1.add("hello");
		keywords1.add("information");
		keywords1.add("monkey");
		keywords1.add("name");
		keywords1.add("yahoo");

		String url1 = "https://www.google.se/";
		String url2 = "https://www.yahoo.com/";
		String url3 = "https://www.facebook.com/";
		String url4 = "https://www.linkedin.com/feed/";
		String url5 = "https://www.google.se/";

		List<String> urls = new ArrayList<String>();
		urls.add(url1);
		urls.add(url2);
		urls.add(url3);
		urls.add(url4);
		urls.add(url5);

		for(String anUrl : urls)
		{
			ThreadedWordCounter.getKeywordCount(keywords1, anUrl);
		}
	}
}
