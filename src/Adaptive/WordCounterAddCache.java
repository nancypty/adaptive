package Adaptive;

import java.io.*;
import java.net.URL;
import java.util.*;


public class WordCounterAddCache {
	
	public int getKeyWordCount(List<String> keywords, String url) {
		int res = 0;
		List<String> content = getContent(url);
		res = countContent(keywords, content);
		return res;
	}


	private List<String> getContent(String url) {
		List<String> res = new ArrayList<String>();
		switch (CacheRefKeeper.getInstance().getResouce(url))
		{
		case 1: //already exist and not old
		{
			System.out.println("Already exist and Not old");
			try {
				res = CacheRefKeeper.getInstance().readHm(url);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					res = readWeb(url);
				} catch (IOException e) {
					e.printStackTrace();
					assert(false);
				}
			}
			break;	
		}
		case 2: //not exist or too old
			//There are 3 things to do
			//1. readWeb
			//2. writeHm
			//3. store content to a file
		{
			System.out.println("Not exist or Too old");
			try {
				//1. readWeb
				res = readWeb(url);
			} catch (IOException e) {
				e.printStackTrace();
				assert(false);
			}
			//2. writeHm
			CacheRefKeeper.getInstance().writeHm(url);

			//3. store content to a file
			CreateFile createfileOb = new CreateFile();
			createfileOb.createFile(res, url);
			break;
		}
		}
		return res;
	}
	
	
	private List<String> readWeb(String url) throws IOException {
		List<String> lines = new ArrayList<String>();
		URL urlOb = new URL(url);
		BufferedReader br = new BufferedReader(
				new InputStreamReader(urlOb.openStream()));
		String line = "";
		while((line = br.readLine()) != null) {
			lines.add(line);
		}
		br.close();
		return lines;
	}



	private int countContent(List<String> keywords, List<String> content) {
		int sum = 0;
		//use a local hashMap store keywords and number of appear
		HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
		for (String keyword : keywords) {
			hashMap.put(keyword, 0);
		}
		for (String s : content) {
			matchKeywords(s, hashMap);
		}
		//traverse hashMap to give result
		for (Map.Entry<String, Integer> entry : hashMap.entrySet()) {
			System.out.println("Word : " + entry.getKey() + "  Count : " + entry.getValue());
			sum += entry.getValue();
		}
		return sum;
	}
	
	
	
	private void matchKeywords(String value, HashMap<String, Integer> hashMap) {
		//remove all punctuation
		value = value.replaceAll("[^a-zA-Z]", " ");
		StringTokenizer tokenizer = new StringTokenizer(value);
		
		while (tokenizer.hasMoreTokens()) {
			String word = tokenizer.nextToken();
			if (hashMap.containsKey(word)) {
				int k = hashMap.get(word).intValue() + 1;
				hashMap.put(word, new Integer(k)); 
			}
		}
	}

}
