package Adaptive;
import java.util.List;

public class ThreadedWordCounter {

	public static int getKeywordCount(List<String> keywords, String url) {
		Thread t = new Thread(
				new Runnable()
				{
					public void run()
					{
						int res = 0;
						res = new WordCounterAddCache().getKeyWordCount(keywords, url);
						System.out.println("The result of getKeyWordCount is " + res);
						
					}
				});
		t.start();
		return 0;

	}

}
